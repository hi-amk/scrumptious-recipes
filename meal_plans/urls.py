from django.urls import path

from meal_plans.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanDetailView,
    MealPlanUpdateView,
    MealPlanDeleteView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplans_list"),
    path("create/", MealPlanCreateView.as_view(), name="mealplan_new"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="mealplan_detail"),
    path("<int:pk>/edit", MealPlanUpdateView.as_view(), name="mealplan_edit"),
    path(
        "<int:pk>/delete", MealPlanDeleteView.as_view(), name="mealplan_delete"
    ),
]
