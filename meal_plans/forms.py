# from django import forms

# from meal_plans.models import MealPlan


# I should be safe commenting out the below MealPlanForm
# given that I'm using a class-based view for MealPlanCreateView
# Doing the scrumptious-receipes project a second time,
# I'm reflecting back on why I wrote this the first time,
# and I believe it's because while creating the MealPlanCreateView,
# the instructions say to reference the RecipeCreateView and how we wrote that
# and back then, we had started off writing a FUNCTION-based view.
# Then, in a future project, we updated that function-based create view
# into a class-based create view.
# in a stretch-goal for one of the projects,
# we were told to clean up our code (the RecipeForm that we no longer needed)
# but I never got to the stretch-goal, so I still had it in my code
# CONCLUSION: the below MealPlanForm is not needed.
# Im fact, I believe this forms.py isn't needed at all
# given that all of my views are class-based views

# class MealPlanForm:
# class MealPlanForm(forms.ModelForm):
#     class Meta:
#         model = MealPlan
#         fields = [
#             "name",
#             "owner",
#             "recipes",
#             "date",
#         ]
