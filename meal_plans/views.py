from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

# from meal_plans.forms import MealPlanForm
from meal_plans.models import MealPlan

from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

# COMMENT
# If user types "/meal_plans/" into the url address at the top of the page
# we DONT want to show the meal plans list page, but prompt to login.
# that's why we add the "LoginRequiredMixin"
class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 10
    # success_url = reverse_lazy("mealplans_list")

    # COMMENT FOR MYSELF
    # code given by Learn as hint.
    # https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/03-template-topics/68-meal-plans.md
    # The below code is what tells Django/database WHICH mealplans to show in the MealPlanListView.
    # If Ariana created her own meal plans, then when Ariana goes to her mealplans_list page, it should ONLY show Ariana's.
    # It Mitch created his own meal plans, then when Mitch goes to his mealplans_list page, it should ONLY show Mitch's.
    # So the below code is saying FILTER the meal plans shown on the MealPlanListView to show ONLY those where owner=requester/user
    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "recipes", "date"]

    # it's not needed because
    # [[return redirect("mealplan_detail", pk=plan.id)]] (last line inside form_valid below)
    # will take care of the redirecting once the new mealplan is successfully created
    # success_url = reverse_lazy("mealplan_detail")

    # ??????? - Learn says to just have the code for now
    # the immediate below code was copy-pasted from recipes/views.py
    # but Learn provides a different form_valid method (see below AFTER this immediately below code block).
    # Per Learn:
    # Because of the way that Django works with the database, there's an extra step to the data saving.
    # If you're in a function view or class view, it doesn't change, really.
    # In a create class view, you'll need to implement the form_valid method to associate the user and, now, save the many-to-many relationships.
    # old code below
    # def form_valid(self, form):
    #     form.instance.owner = self.request.user
    #     return super().form_valid(form)

    # LINE BY LINE EXPLANATION
    # https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/03-views-revealed.md
    # Custom handling of saving the form
    def form_valid(self, form):
        # Save the meal plan, but don't put it in the database
        plan = form.save(commit=False)
        # Assign the owner to the meal plan
        plan.owner = self.request.user
        # Now, save it to the database
        plan.save()
        # Save all of the many-to-many relationships
        form.save_m2m()
        # Redirect to the detail page for the meal plan
        return redirect("mealplan_detail", pk=plan.id)

    # FURTHER GENERAL FORMAT/EXPLANATION FROM:
    # https://learn-2.galvanize.com/cohorts/3189/blocks/1859/content_files/build/03-django-two-shot/67-django-two-shot.md
    # If you are using class views, you can do this by writing your own form_valid method.
    # The general form of a method that assigns the current user to a User property looks like this.

    # def form_valid(self, form):
    #     item = form.save(commit=False)
    #     item.user_property = self.request.user
    #     item.save()
    #     return redirect("some_view")


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    # COMMENT FOR MYSELF
    # code given by Learn as hint.
    # https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/03-template-topics/68-meal-plans.md
    # The below code is what tells Django/database WHICH mealplans to show in the MealPlanListView.
    # If Ariana created her own meal plans, then when Ariana goes to her mealplans_list page, it should ONLY show Ariana's.
    # It Mitch created his own meal plans, then when Mitch goes to his mealplans_list page, it should ONLY show Mitch's.
    # So the below code is saying FILTER the meal plans shown on the MealPlanListView to show ONLY those where owner=requester/user
    # -- confirmed by John's presentation, that queryset is needed for DetailView
    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "recipes", "date"]

    # COMMENT FOR MYSELF
    # this original success_url below, which comes (copy-pasted) from RecipeUpdateView in recipes/views.py,
    # takes me to "mealplans_list" once the meal plan is updated successfully
    # success_url = reverse_lazy("mealplans_list")
    # this would be fine IF everytime we successully update a mealplan_detail, we want to show the mealplans_list page
    # HOWEVER, let's say once a mealplan_detail is successfully updated, we want it to go back to that same (updated) mealplan_detail
    # the below code is the code that tells WHICH mealplan_detail to show! YAY
    def get_success_url(self) -> str:
        return reverse_lazy("mealplan_detail", args=[self.object.id])

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("mealplans_list")

    # COMMENT FOR MYSELF
    # code given by Learn as hint.
    # https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/03-template-topics/68-meal-plans.md
    # The below code is what tells Django/database WHICH mealplans to show in the MealPlanListView.
    # If Ariana created her own meal plans, then when Ariana goes to her mealplans_list page, it should ONLY show Ariana's.
    # It Mitch created his own meal plans, then when Mitch goes to his mealplans_list page, it should ONLY show Mitch's.
    # So the below code is saying FILTER the meal plans shown on the MealPlanListView to show ONLY those where owner=requester/user
    # -- confirmed by John's presentation, that queryset is needed for DetailView
    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
