from django import forms

# from recipes.models import Recipe


# we originally needed this when we had function-based views in recipes/views.py.
# but once we moved to class-based views (RecipteCreateView and RecipeUpdateView),
# these are no longer needed
# class RecipeForm(forms.ModelForm):
#     class Meta:
#         model = Recipe
#         fields = [
#             "name",
#             "author",
#             "description",
#             "image",
#         ]


from recipes.models import Rating


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]
