from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

# we originally needed to import RecipeForm when we had function-based views.
# but once we moved to class-based views (RecipteCreateView and RecipeUpdateView),
# these are no longer needed
from recipes.forms import RatingForm  # , RecipeForm

from recipes.models import Recipe, Ingredient, ShoppingItem
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 4


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        # context: https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/73-shopping-lists.md
        # Create a new empty list and assign it to a variable
        shoppinglist = []
        # For each item in the user's shopping items, which we
        # can access through the code
        # self.request.user.shopping_items.all()
        # (self is passed in as a parameter to this get_context_data method)
        # (request.user is the current user)
        # (shoppingitems is the related_name we gave to the user property in the ForeignKey within ShoppingItem model)
        # (.all just to get all of shoppingitem instances)
        for item in self.request.user.shopping_items.all():
            # Add the shopping item's food to the list
            shoppinglist.append(item.food_item)
        # Put that list into the context
        context["shopping_list"] = shoppinglist

        # Strategy: Get the resize request, if it exists, from the request.GET dictionary and put it into the context
        # The self.request.GET property is a dictionary
        # Get the value out of there associated with the key "servings"
        # Store in the context dictionary with the key "servings"
        # (this key is the "name= " in the input tag that I myself assigned it on recipes/detail.html -- see around line 27)
        context["servings"] = self.request.GET.get("servings_resize_request")

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]

    # The below 4 lines of code work just fine IF we want to
    # redirect user to "recipes_list" once recipe is successfully created
    # but the longer version of form_valid method below allows us to
    # redirect user to the specific "recipe_detail" page

    # success_url = reverse_lazy("recipes_list")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

    def form_valid(self, form):
        # Save the recipe, but don't put it in the database
        recipe = form.save(commit=False)
        # Assign the author to the meal plan
        recipe.author = self.request.user
        # Now, save it to the database
        recipe.save()
        # Redirect to the detail page for the meal plan
        return redirect("recipe_detail", pk=recipe.id)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image", "servings"]
    # success_url = reverse_lazy("recipes_list")

    def get_success_url(self) -> str:
        return reverse_lazy("recipe_detail", args=[self.object.id])


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


# This view handles only HTTP POST requests, which means there's no HTML template for it.
# It should create a ShoppingItem instance in the database based on the current user and the value of the submitted "ingredients_id" value.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/65-shopping-lists.md
# To make it so a function view only handles POST requests, you can use the http_require_methods decorator.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/67-shopping-lists.md
# (this affects the behavior of the page when the user types /recipes/shopping_items/create on the url address bar)
# (with this @require_http_methods, the user will get a 405 error "The page isn't working")
# (without this @require_http_methods, idk what it would show, but the developer sees a yellow page of sadness: )
# ("DoesNotExist at /recipes/shopping_items/create/"           "Ingredient matching query does not exist."")
@require_http_methods(["POST"])
def create_shopping_item(request):
    # Get the value for the "ingredient_id" from the request.POST dictionary using the "get" method
    # ("ingredient_id" is defined by us in detail.html, around line 64)
    # (request is the data/info being passed in (see argument))
    # (.get is a python built-in method that allows us to access a dictionary's value given the dictionary's key -- Learn told us that request.POST is a dictionary)
    ingredient_id = request.POST.get("ingredient_id")
    # Get the specific ingredient from the Ingredient model. (id=the value from the dictionary)
    # (ingredient is just an arbitrary variable name to store the current ingredient we are dealing with)
    # (Ingredient.objects is where all the instances of the Ingredient class are -- this is by Django's design, built-in (nothing we created).)
    # (.get is the method that lets us access the specific instance of the ingredient given the ingredient_id -- which we already obtained above)
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # Get the current user which is stored in request.user
    # (each request is tied to a user. we're just storing that into the variable currentuser)
    currentuser = request.user
    try:
        # Create the new shopping item in the database
        # (ShoppingItem.objects is where all the instances of the ShoppingItem class are - this is by Django's design, built-in (nothing we created).)
        # (The .create method is also Django's built-in method to create an instance of the ShoppingItem)
        # (The ShoppingItem class expects 1)a user and 2)a food item! this is the reason for the below arguments)
        ShoppingItem.objects.create(
            # food_item= the food item on the ingredient,
            food_item=ingredient.food,
            # user= the current user
            user=currentuser,
        )
    # Raised if someone tries to add the same food twice, just ignore it
    except IntegrityError:
        pass
    # Go back to the recipe page with a redirect to the name of the registered recipe detail path
    return redirect(
        # name of the registered recipe detail path,
        "recipe_detail",
        # pk=id of the ingredient's recipe
        # (i.e this ingredient's instance has a recipe as a field (refer back to the Ingredient class) and that recipe also has a field called id in the database assigned by Django)
        pk=ingredient.recipe.id,
    )


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


# This view handles only HTTP POST requests, which means there's no HTML template for it.
# It should create a ShoppingItem instance in the database based on the current user and the value of the submitted "ingredients_id" value.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/65-shopping-lists.md
# To make it so a function view only handles POST requests, you can use the http_require_methods decorator.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/67-shopping-lists.md
# (this affects the behavior of the page when the user types /recipes/shopping_items/create on the url address bar)
# (with this @require_http_methods, the user will get a 405 error "The page isn't working")
@require_http_methods(["POST"])
def delete_all_shopping_items(request):
    user = request.user
    # Delete all of the shopping items for the user
    # using code like
    # ShoppingItem.objects.filter(user=the current user).delete()
    ShoppingItem.objects.filter(user=user).delete()

    # Go back to the shopping item list with a redirect
    # to the name of the registered shopping item list
    # path with code like this
    return redirect("shopping_items_list")
